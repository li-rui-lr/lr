export  default  formatDate(date) {
    return `${date.getMonth() + 1}/${date.getDate()}`;
}
//上面是第一种方法


//下面是第二种方法
/**
 * @desc 格式化时间
 * @parms data 时间戳
 */
// export default formatDate(data){
//   let years = new Date(data).getFullYear();  //年
//   let months =new Date(data).getMonth();//月
   
//    //years>10 &&years<10

//   return years+'-'+months;
// }


//第三种方法

// const  obj ={
//     formatDate:formatDate
// }
// export default obj;
// obj.formatDate
