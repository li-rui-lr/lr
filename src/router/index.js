import Vue from 'vue'
import VueRouter from 'vue-router'
import IronLclIndex from "../pages/trainLcl/IronLclIndex.vue"
import IronLclline from "../pages/trainLcl/IronLclline.vue"
import IronLclCost from "../pages/trainLcl/IronLclCost.vue"
import IronLclService from "../pages/trainLcl/IronLclService.vue"
import IronLclSubmitorder from "../pages/trainLcl/IronLclSubmitorder.vue"
import IronLclInformation from "../pages/trainLcl/IronLclInformation.vue"
import AirTransportIndex from "../pages/aircraft/AirTransportIndex.vue"
import AirTransportLine from "../pages/aircraft/AirTransportLine.vue"
import AirTransportServe from "../pages/aircraft/AirTransportServe.vue"
import AirTransportCost from "../pages/aircraft/AirTransportCost.vue"
import AirTransportInformation from "../pages/aircraft/AirTransportInformation.vue"
import AirTransportSubmitorder from "../pages/aircraft/AirTransportSubmitorder.vue"
import AutomobileTransportIndex from "../pages/automobile/AutomobileTransportIndex.vue"
import AutomobileTransportLine from "../pages/automobile/AutomobileTransportLine.vue"
import AutomobileTransportCost from "../pages/automobile/AutomobileTransportCost.vue"
import AutomobileTransportService from "../pages/automobile/AutomobileTransportService.vue"
import AutomobileTransportInformation from "../pages/automobile/AutomobileTransportInformation.vue"
import AutomobileTransportSubmitorder from "../pages/automobile/AutomobileTransportSubmitorder.vue"

Vue.use(VueRouter)

const routes = [



  {
    path: '/',
    redirect:'ironLclIndex' 
  },
  {
    path: '/automobileTransportIndex',
    name: 'AutomobileTransportIndex',
    component: AutomobileTransportIndex
  },
  {
    path: '/automobileTransportLine',
    name: 'AutomobileTransportLine',
    component: AutomobileTransportLine
  },
  {
    path: '/automobileTransportCost',
    name: 'AutomobileTransportCost',
    component: AutomobileTransportCost
  },
  {
    path: '/automobileTransportService',
    name: 'AutomobileTransportService',
    component: AutomobileTransportService
  },
  {
    path: '/automobileTransportInformation',
    name: 'AutomobileTransportInformation',
    component: AutomobileTransportInformation
  },
  {
    path: '/automobileTransportSubmitorder',
    name: 'AutomobileTransportSubmitorder',
    component: AutomobileTransportSubmitorder
  },










  {
    path: '/ironLclIndex',
    name: 'IronLclIndex',
    component: IronLclIndex
  },
  {
    path: '/ironLclInformation',
    name: 'IronLclInformation',
    component: IronLclInformation
  },
  {
    path: '/ironLclSubmitorder',
    name: 'IronLclSubmitorder',
    component: IronLclSubmitorder
  },
  {
    path: '/ironLclService',
    name: 'IronLclService',
    component: IronLclService
  },
  {
    path: '/ironLclline',
    name: 'IronLclline',
    component: IronLclline
  },
  {
    path: '/ironLclCost',
    name: 'IronLclCost',
    component: IronLclCost
  },
  {
    path:"/airTransportInformation",
    name:"AirTransportInformation",
    component: AirTransportInformation
  },
   {
    path: '/airTransportIndex',
    name: 'AirTransportIndex',
    component: AirTransportIndex
  },
   {
    path: '/airTransportLine',
    name: 'AirTransportLine',
    component: AirTransportLine
  },
   {
    path: '/airTransportCost',
    name: 'AirTransportCost',
    component: AirTransportCost
  },
   {
    path: '/airTransportServe',
    name: 'AirTransportServe',
    component: AirTransportServe
  },
   {
    path: '/airTransportSubmitorder',
    name: 'AirTransportSubmitorder',
    component: AirTransportSubmitorder
  },
 
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
